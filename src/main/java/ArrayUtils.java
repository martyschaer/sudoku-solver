import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ArrayUtils {

    private static final Set<Integer> validNumbers = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));

    public static int[] compress(int[] numbers) {
        return Arrays.stream(numbers)
                .filter(n -> n > 0)
                .sorted()
                .distinct()
                .toArray();
    }

    public static int[] distinct(int[]... arrays) {
        int totalLength = 0;
        for (int[] a : arrays) {
            totalLength += a.length;
        }

        int[] output = new int[totalLength];

        int i = 0;
        for (int[] a : arrays) {
            for (int n : a) {
                output[i++] = n;
            }
        }
        return compress(output);
    }

    /**
     * Returns Set A with all elements of B removed.
     */
    public static int[] difference(int[] a, int[] b){
        Set<Integer> A = new HashSet<>(a.length);
        for(int n : a){
            A.add(n);
        }

        Set<Integer> B = new HashSet<>(b.length);
        for(int n : b){
            B.add(n);
        }

        A.removeAll(B);

        return A.stream().mapToInt(Integer::intValue).toArray();
    }
}
