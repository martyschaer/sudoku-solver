import java.awt.*;
import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Board {
    private static final char[] printable = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
    private static final int[] alphabet = IntStream.range(1, printable.length).toArray();

    private int[][] board;
    private final boolean[][] mutable;
    private final int side;
    private final int sector;
    private final int[] validValues;

    private List<Integer>[][] latestCandidates;

    private Board(int[][] board, boolean[][] mutable, int side, int sector, int[] validValues){
        this.board = board;
        this.mutable = mutable;
        this.side = side;
        this.sector = sector;
        this.validValues = validValues;
    }

    public static Board of(String input){
        input = input.replaceAll("([|\\s])", "");
        validate(input);

        int[][] board;
        boolean[][] mutable;
        int side;
        int sector;
        int[] validValues;

        side = (int)Math.sqrt(input.length());
        sector = (int)Math.sqrt(side);
        board = new int[side][side];
        mutable = new boolean[side][side];
        validValues = Arrays.copyOfRange(alphabet, 0, side);

        char[] chars = input.toCharArray();

        for(int i = 0; i < (int)Math.pow(side, 2); i++){
            char c = chars[i];
            int row = i / side;
            int col = i % side;
            if(c == '_' || c == '.'){
                mutable[row][col] = true;
                continue;
            }
            int n = printableToValue(c);
            board[row][col] = n;
            mutable[row][col] = false;
        }
        return new Board(board, mutable, side, sector, validValues);
    }

    private static void validate(String input){
        if(!isQuadratic(input.length())){
            throw new IllegalArgumentException("Invalid input length");
        }
    }

    private static boolean isQuadratic(int number){
        double sqrt = Math.sqrt(number);
        // integer check
        return ((sqrt - (int)sqrt) < 0.00001);
    }

    public int get(int row, int col){
        return board[row][col];
    }

    public void set(int row, int col, int val){
        // do not overwrite given fields
        if(mutable[row][col]){
            board[row][col] = val;
        }else{
            throw new IllegalArgumentException(String.format("You are not allowed to overwrite a given value @[%d, %d]", row, col));
        }
    }

    public int getSide(){
        return this.side;
    }

    public List<Integer>[][] getLatestCandidates(){
        return latestCandidates;
    }

    /**
     * Gets all the numbers allowed at a given position
     */
    public int[] getCandidates(int row, int col){
        // all possible values minus the present numbers, giving the allowed numbers
        return ArrayUtils.difference(validValues,
                ArrayUtils.distinct(        // gives the set of all present numbers
                        this.getCol(col),
                        this.getRow(row),
                        this.getSector(row / this.sector, col / this.sector)
                )
        );
    }

    public int[] getRow(int row){
        return board[row];
    }

    public int[] getCol(int col){
        int[] column = new int[this.side];
        for(int i = 0; i < this.side; i++){
            column[i] = board[i][col];
        }
        return column;
    }

    public int[] getSector(int sectorRow, int sectorCol){
        int[] sector = new int[this.side];
        int i = 0;
        for(int rowAdd = 0; rowAdd < this.sector; rowAdd++){
            for(int colAdd = 0; colAdd < this.sector; colAdd++){
                int row = sectorRow * this.sector + rowAdd;
                int col = sectorCol * this.sector + colAdd;
                sector[i++] = board[row][col];
            }
        }
        return sector;
    }

    public boolean isValid(){
        boolean valid = true;
        for(int i = 0; i < this.side; i++){
            valid &= isRowValid(i);
            valid &= isColValid(i);
        }
        for(int sectorRow = 0; sectorRow < this.sector; sectorRow++){
            for(int sectorCol = 0; sectorCol < this.sector; sectorCol++){
                valid &= isSectorValid(sectorRow, sectorCol);
            }
        }
        return valid;
    }

    private boolean isRowValid(int row){
        // all valid values + 1 for 0 when not filled in
        byte[] occurences = new byte[validValues.length + 1];
        for(int col = 0; col < this.side; col++){
            occurences[board[row][col]]++;
        }
        return areOccurencesValid(occurences);
    }

    private boolean isColValid(int col){
        byte[] occurences = new byte[validValues.length + 1];
        for(int row = 0; row < this.side; row++){
            occurences[board[row][col]]++;
        }
        return areOccurencesValid(occurences);
    }

    private boolean isSectorValid(int sectorRow, int sectorCol){
        byte[] occurences = new byte[validValues.length + 1];
        for(int rowAdd = 0; rowAdd < this.sector; rowAdd++){
            for(int colAdd = 0; colAdd < this.sector; colAdd++){
                int row = sectorRow * this.sector + rowAdd;
                int col = sectorCol * this.sector + colAdd;
                occurences[board[row][col]]++;
            }
        }
        return areOccurencesValid(occurences);
    }

    private boolean areOccurencesValid(byte[] occurences){
        // all valid values + 1 for zero if not set
        // even though 0 is not counted, because we start at one here
        for(int i = 1; i < this.validValues.length + 1; i++){
            if(occurences[i] > 1){
                return false;
            }
        }
        return true;
    }

    public boolean isSolved(){
        boolean solved = true;
        for(int i = 0; i < this.side; i++){
            solved &= isRowSolved(i);
            solved &= isColSolved(i);
        }
        for(int sectorRow = 0; sectorRow < this.sector; sectorRow++){
            for(int sectorCol = 0; sectorCol < this.sector; sectorCol++){
                solved &= isQuadrantSolved(sectorRow, sectorCol);
            }
        }
        return solved;
    }

    private boolean isRowSolved(int row){
        byte[] occurences = new byte[this.validValues.length + 1];
        for(int col = 0; col < this.side; col++){
            occurences[board[row][col]]++;
        }
        return areOccurencesSolved(occurences);
    }

    private boolean isColSolved(int col){
        byte[] occurences = new byte[this.validValues.length + 1];
        for(int row = 0; row < this.side; row++){
            occurences[board[row][col]]++;
        }
        return areOccurencesSolved(occurences);
    }

    private boolean isQuadrantSolved(int sectorRow, int sectorCol){
        byte[] occurences = new byte[this.validValues.length + 1];
        for(int rowAdd = 0; rowAdd < this.sector; rowAdd++){
            for(int colAdd = 0; colAdd < this.sector; colAdd++){
                int row = sectorRow * this.sector + rowAdd;
                int col = sectorCol * this.sector + colAdd;
                occurences[board[row][col]]++;
            }
        }
        return areOccurencesSolved(occurences);
    }

    private boolean areOccurencesSolved(byte[] occurences){
        for(int i = 1; i < this.validValues.length + 1; i++){
            if(occurences[i] != 1){
                return false;
            }
        }
        return true;
    }

    /**
     * Recursively solve the puzzle by applying candidates with 100% certainty as a solution
     */
    public Board solve(){
        return solve(initializeCandidates());
    }

    private Board solve(List<Integer>[][] candidates){
        if(this.isSolved()){
            return this;
        }

        boolean changedBoard = false;

        // apply candidates where only one qualifies
        for(int row = 0; row < this.side; row++){
            for(int col = 0; col < this.side; col++){
                if(candidates[row][col] != null && candidates[row][col].size() == 1){
                    this.set(row, col, candidates[row][col].get(0));
                    candidates[row][col].remove(0);
                    changedBoard = true;
                }
            }
        }

        // no candidates were applied
        if(!changedBoard){
            Point rowCol = findSmallestPossibilityLocation(candidates);
            List<List<Integer>[][]> branches = generateBranches(candidates, rowCol);
            for(List<Integer>[][] branch : branches){
                int[][] previousBoard = this.board.clone();
                try{
                    System.out.printf("Attempting branching at [%02d, %02d] with %s%n", rowCol.x, rowCol.y, candidates[rowCol.x][rowCol.y].toString());
                    return solve(branch);
                }catch (StackOverflowError e){
                    this.board = previousBoard;
                    continue;
                }
            }
        }

        // generate new candidates
        for(int row = 0; row < this.side; row++){
            for(int col = 0; col < this.side; col++){
                // only if there is no solution already present
                if(this.get(row, col) == 0) {
                    candidates[row][col] = Arrays.stream(this.getCandidates(row, col)).boxed().collect(Collectors.toList());
                }
            }
        }
        latestCandidates = candidates;
        return solve(candidates);
    }

    private Point findSmallestPossibilityLocation(List<Integer>[][] candidates){
        Point rowCol = new Point(0, 0);
        int smallestPossibilities = Integer.MAX_VALUE;
        for(int row = 0; row < this.side; row++){
            for(int col = 0; col < this.side; col++){
                if(candidates[row][col] != null && candidates[row][col].size() < smallestPossibilities){
                    smallestPossibilities = candidates[row][col].size();
                    rowCol = new Point(row, col);
                }
            }
        }
        return rowCol;
    }

    private List<List<Integer>[][]> generateBranches(List<Integer>[][] candidates, Point smallestPossibilities){
        List<List<Integer>[][]> branches = new ArrayList<>();
        int row = smallestPossibilities.x;
        int col = smallestPossibilities.y;
        List<Integer> possibilities = new ArrayList<>(candidates[row][col]);
        for(Integer possibility : possibilities){
            List<Integer>[][] branch = candidates.clone();
            branch[row][col].clear();
            branch[row][col].add(possibility);
            branches.add(branch);
        }
        return branches;
    }

    private List<Integer>[][] initializeCandidates() {
        List<Integer>[][] candidates = new ArrayList[this.getSide()][this.getSide()];
        for(int row = 0; row < this.getSide(); row++){
            for(int col = 0; col < this.getSide(); col++){
                if(this.get(row, col) == 0){
                    // add the list of candidates for a given position
                    candidates[row][col] = Arrays.stream(this.getCandidates(row, col)).boxed().collect(Collectors.toList());
                }else{
                    // given fields should not have candidates
                    candidates[row][col] = null;
                }
            }
        }
        return candidates;
    }

    public String toString(boolean onlyMutable){
        final String horizontalSeparator = (("+" + "-".repeat(this.sector)).repeat(this.sector)).substring(1) + "\n";
        final String vertcalSeparator = "|";
        StringBuilder sb = new StringBuilder();
        for (int row = 0; row < this.side; row++) {
            if (row % this.sector == 0 && row > 0) {
                sb.append(horizontalSeparator);
            }
            for (int col = 0; col < this.side; col++) {
                if (col % this.sector == 0 && col > 0) {
                    sb.append(vertcalSeparator);
                }
                int value = board[row][col];
                if(value == 0 || (!this.mutable[row][col] && onlyMutable)){
                    sb.append(".");
                }else{
                    sb.append(valueToPrintable(value));
                }
            }
            sb.append("\n");

        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return toString(false);
    }

    private static Character valueToPrintable(int value){
        return Board.printable[value];
    }

    private static int printableToValue(Character printable){
        for(int i = 0; i < Board.printable.length; i++){
            if(printable.equals(Board.printable[i])){
                return i;
            }
        }
        throw new IllegalArgumentException("Could not find this value: " + printable);
    }
}
