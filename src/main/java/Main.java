import java.util.List;

public class Main {
    public static void main(String[] args) {
        Board board = Board.of(Examples.SMALL_MEDIUM);

        // try to solve board.
        if(!board.isValid()){
            throw new IllegalArgumentException("Given Sudoku is not valid to begin with!");
        }
        System.out.println("Input:");
        System.out.println(board.toString());

        try {
            long start = System.currentTimeMillis();
            Board solved = board.solve();
            long end = System.currentTimeMillis();
            System.out.printf("Solved in %.3f seconds:%n", (end-start)/1000.0);
            System.out.println(solved);
        }catch(StackOverflowError e){
            System.out.println("Couldn't solve, try giving a larger stack... last state:");
            System.out.println(board.toString(false));
            System.out.println("Latest candidates:");
            for(int row = 0; row < board.getSide(); row++){
                for(int col = 0; col < board.getSide(); col++){
                    List<Integer> candidates = board.getLatestCandidates()[row][col];
                    if(candidates != null && candidates.size() > 0){
                        System.out.printf("[%02d, %02d] => %s%n", row, col, candidates.toString());
                    }
                }
            }
        }
    }
}
