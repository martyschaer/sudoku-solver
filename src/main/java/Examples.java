/**
 * Some example Sudoku boards
 * Pipes (|) are optional for better readability
 * Underscores (_) or Dots (.) represent blank spaces.
 *
 * Can be generated with help of www.sudoku-puzzles-online.com
 */
public class Examples {
    // 9x9
    public static final String WIKIPEDIA =
                    "53_|_7_|___" +
                    "6__|195|___" +
                    "_98|___|_6_" +
                  //----+---+---- +
                    "8__|_6_|__3" +
                    "4__|8_3|__1" +
                    "7__|_2_|__6" +
                  //----+---+---- +
                    "_6_|___|28_" +
                    "___|419|__5" +
                    "___|_8_|_79";
    public static final String GENERATED = "8_6__19__4_1_9__2_____7481_1___23__6_2_4_6_9____98__3__1__37_49__71__38______21_7";

    public static final String SMALL_MEDIUM = "....5.....14....629.6..1...5.2.3..8..7......4.3..2.9....5.9....6.7...8.........3.";

    // 16x16
    public static final String LARGE_EASY =
                    "__BA|F_4_|89_D|____" +
                    "C___|___E|7_A_|_549" +
                    "_7_2|__18|____|6___" +
                    "__FE|_B3_|2___|1__8" +
                  //-----+----+----+----- +
                    "8_G_|_E__|4___|_F62" +
                    "___C|A4__|D6_F|3_5_" +
                    "_FD6|____|A_8_|_1_B" +
                    "___7|_C_9|___G|A_E_" +
                  //-----+----+----+----- +
                    "____|_86_|E_F_|_C__" +
                    "E__D|___A|_C54|__8F" +
                    "__A_|___B|_8__|_9_3" +
                    "___9|3D__|__B_|_4_5" +
                  //-----+----+----+----- +
                    "___F|_9A_|C__5|BD__" +
                    "7___|G__1|__3B|____" +
                    "_8_B|75__|F___|9A__" +
                    "A_6_|__DC|9_4_|_E_7";

    public static final String LARGE_MEDIUM = "F6..923C.A..D....5A..8EF..........DCB..A9....E.F91.7...4B.C..2A.....3D6.E...F.8.D............B.5..32..C.F..6...4....E..9..4..DC..AC6...D.....7..54.1.CA6.9........F.G.9E638.....8.73..F.D5.B............5E....6..E6.....2C.A.F5DB....G..3..7EA...8.......DGF....";
}
